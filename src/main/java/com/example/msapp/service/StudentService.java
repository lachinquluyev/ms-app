package com.example.msapp.service;

import com.example.msapp.dto.StudentResponse;
import com.example.msapp.dto.request.CreateStudentRequest;
import com.example.msapp.dto.request.UpdateStudentRequest;
import com.example.msapp.exception.StudentNotFoundException;
import com.example.msapp.mapper.StudentMapper;
import com.example.msapp.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {

    private final StudentRepository repository;
    private final StudentMapper mapper;
    public void createStudent(CreateStudentRequest studentRequest){
        repository.save(mapper.mapRequestToStudent(studentRequest));
    };
    public void updateStudent(Long id, UpdateStudentRequest updaterequest){
        var student=repository.findById(id)
                .orElseThrow(()->exStudentNotFound(id));
        mapper.updateRequestToEntity(student,updaterequest);
        repository.save(student);
    }
    public List<StudentResponse> getAllStudents(){
        return repository.findAll()
                .stream()
                .map(mapper::mapEntityToResponse)
                .collect(Collectors.toList());
    }
    public StudentResponse getStudent(Long id){
        var student =  repository.findById(id)
                .orElseThrow(()->exStudentNotFound(id));
        return mapper.mapEntityToResponse(student);
    }
    public void deleteStudent (Long id){
        var student =  repository.findById(id)
                .orElseThrow(()->exStudentNotFound(id));
        repository.deleteById(student.getId());
    }
    public StudentNotFoundException exStudentNotFound(Long id){
        throw new StudentNotFoundException("Student Not Found by ID " + id);
    }
}
