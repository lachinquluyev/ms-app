package com.example.msapp.exception;

public class StudentNotFoundException extends RuntimeException{

    public StudentNotFoundException (String message){
        super(message);
    }
}
