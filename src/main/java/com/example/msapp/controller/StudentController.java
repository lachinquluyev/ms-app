package com.example.msapp.controller;

import com.example.msapp.dto.StudentResponse;
import com.example.msapp.dto.request.CreateStudentRequest;
import com.example.msapp.dto.request.UpdateStudentRequest;
import com.example.msapp.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public void createStudent(@RequestBody @Valid CreateStudentRequest request){
        studentService.createStudent(request);
    }

    @GetMapping
    public List<StudentResponse> getAllStudents(){
        return studentService.getAllStudents();
    }

    @GetMapping("/{id}")
    public StudentResponse getStudent(@PathVariable Long id){
      return studentService.getStudent(id);
    }
    @PutMapping("/{id}")
    public void UpdateStudent(@PathVariable Long id, @RequestBody UpdateStudentRequest request){
        studentService.updateStudent(id, request);
    }
    @DeleteMapping("/{id}")
    public void DeleteStudent(@PathVariable Long id){
        studentService.deleteStudent(id);
    }
}
