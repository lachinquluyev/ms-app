package com.example.msapp.dto.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateStudentRequest {
    @NotBlank(message = "Name must not be empty")
    private String name;
    @NotBlank(message = "SurName must not be empty")
    private String surname;
    @NotBlank(message = "Group_Name must not be empty")
    private String groupName;
}
