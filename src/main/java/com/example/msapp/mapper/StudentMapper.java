package com.example.msapp.mapper;

import com.example.msapp.dto.StudentResponse;
import com.example.msapp.dto.request.CreateStudentRequest;
import com.example.msapp.dto.request.UpdateStudentRequest;
import com.example.msapp.model.Student;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface StudentMapper {
    @Mapping(target = "id", ignore = true)
    Student mapRequestToStudent (CreateStudentRequest studentRequest);
    StudentResponse mapEntityToResponse(Student student);
    Student updateRequestToEntity(@MappingTarget Student student, UpdateStudentRequest updaterequest);
}
